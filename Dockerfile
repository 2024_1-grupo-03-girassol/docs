FROM python:3.10
RUN pip install mkdocs mkdocs-material mkdocs-glightbox
COPY . /docs
WORKDIR /docs
