

# Projeto Integrador 2 - Grupo 3 - Girassol

## Requisitos
Docker

## Configuração

Caso instalado o docker desktop abra-o

Execute `make docs` para acessar o servidor. Para construir os estáticos, execute `make build-docs`.


## Adição de novos documentos

1. Adicionar novo documento na pasta `./docs`
2. Adicionar o nome do arquivo no `mkdocs.yml`

``
nav:
  - Home: 'index.md'
  - 'Semestres Anteriores': 'anteriores.md'

``
3. testar localmente
# Visualizando 

Para visualizar os relatórios, acessar :

[https://2024_1-grupo-03-girassol.gitlab.io/docs/requisitos-arquitetura/](https://2024_1-grupo-03-girassol.gitlab.io/docs/requisitos-arquitetura/)
