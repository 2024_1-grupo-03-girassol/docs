# **Fase 3: Projeto e construção de subsistemas da solução proposta**

A seção 03 "Projeto de subsistemas da solução proposta" se refere à fase
03 do ciclo de vida do projeto e tem por objetivo detalhar o projeto de
subsistemas que compõe a solução. Dentre os aspectos a serem abordados
nesta seção, destacam-se:

- Identificação de critérios/formulações/normas que subsidiam a
  elaboração de projeto

- Uso de modelos, cálculos matemáticos, simulação, testes
  computacionais, etc que subsidiem o desenvolvimento do subsistema;

- Durante a fases de projeto, realiza-se a escolha de componentes,
  tecnologias, materiais, etc, que poderão ser empregados no
  desenvolvimento do produto. É importante que toda a escolha de
  componentes seja baseada em matrizes de decisão, para que o
  projetista tenha clareza das opções disponíveis;

- Durante as fases do projeto, é importante a realização de análises
  críticas dos resultados, de forma a avaliar se a solução projetada
  atende os critérios de projeto, se os elementos do projeto e/ou
  decisões tomadas implicam em "superdimensionamento" dos elementos
  para o propósito do projeto;

- Desenvolvimento de documentação técnica geral do projeto e
  específica de cada subsistema.

  1.  # **Projeto do subsistema 01**

Em um subsistema, pode ser necessário o projeto de um ou mais elementos.
Nesse sentido, detalhar em cada subseção abaixo, o projeto do elemento.

# **Projeto do elemento 01**

Detalhar o projeto do elemento 01 do subsistema 01.

# **Projeto do elemento 02**

Detalhar o projeto do elemento 02 do subsistema 01.

# **Projeto do subsistema 02**

Em um subsistema, pode ser necessário o projeto de um ou mais elementos.
Nesse sentido, detalhar em cada subseção abaixo, o projeto do elemento.

# **Projeto do elemento 01**

Detalhar o projeto do elemento 01 do subsistema 02.

# **Projeto do elemento 02**

Detalhar o projeto do elemento 02 do subsistema 02.

# **Projeto do subsistema 03**

Em um subsistema, pode ser necessário o projeto de um ou mais elementos.
Nesse sentido, detalhar em cada subseção abaixo, o projeto do elemento.

# **Projeto do elemento 01**

Detalhar o projeto do elemento 01 do subsistema 03.

# **Projeto do elemento 02**

Detalhar o projeto do elemento 02 do subsistema 03.
