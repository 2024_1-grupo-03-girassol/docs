# **Introdução**

A seção de Introdução se refere à fase 01 do ciclo de vida do projeto e
tem por objetivo apresentar, de forma refinada, o problema abordado no
projeto.

# **Detalhamento do problema**

Apresentar o problema

# **Levantamento de normas técnicas relacionadas ao problema**

Caso existam, apresentar normas técnicas diretamente associadas ao
problema abordado pela equipe.

# **Identificação de solução comerciais**

Realizar uma pesquisa de soluções já existentes no mercado. É
interessante realizar uma comparação (_benchmarking_) entre as soluções já
existente.

**Tabela 1: Equipe**

  --------------------------------------------------------------------------
               **Solução    **Solução                            **Solução
               01**         02**                                 N**
  ------------ ------------ ------------ ------------ ---------- -----------
  **Critério   Avaliação da Avaliação da
  01**         solução 01   solução 02
               quanto ao    quanto ao
               critério 01  critério 01

  **Critério
  02**



  **Critério
  N**
  --------------------------------------------------------------------------

# **Objetivo geral do projeto**

Assegurar o bom andamento de um projeto e desenvolvimento, conforme
diretrizes gerais de qualidade.

# **Objetivo específicos do projeto em cada área**

### Objetivo específico de Estrutura

Desenvolver a estrutura física que suportará os painéis solares, levando em consideração fatores como vento, peso dos painéis e carga solar, garantindo que a estrutura esteja funcionando corretamente ao longo do tempo, trabalhando para otimizar a eficiência da estrutura, garantindo que os painéis solares estejam posicionados da melhor maneira possível para capturar a maior quantidade de luz solar, e certificando-se de que a estrutura seja segura para operação e que atenda aos padrões de segurança estabelecidos.

### Objetivo específico de Software

Desenvolver algoritmos que garantam o posicionamento preciso dos painéis solares ao longo do dia, com o objetivo de maximizar a captura de energia solar. Além disso, planejamos criar um software que possibilite a configuração remota do equipamento pela Internet, integrando-se ao sistema embarcado do rastreador solar. Isso também envolve o desenvolvimento de uma aplicação web com uma interface amigável, permitindo aos usuários visualizar os dados captados e controlar o rastreador solar em tempo real.

### Objetivo específico de Energia e Eletrônica

Garantir o fornecimento eficiente e seguro de energia para os motores. Gerenciar o funcionamento geral do sistema de *tracker*, incluindo a comunicação com sensores, motores, receber dados de sensores de posição solar, controlar o movimento dos motores do *tracker* para manter os painéis solares direcionados para o sol (modo automático e manual) e principalmente monitorar o desempenho do sistema e detectar as falhas e fornecer as suas soluções de maneira rápida e eficaz.



