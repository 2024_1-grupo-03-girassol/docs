# **Referências Bibliográficas**

ABNT. **Instalações elétricas de baixa tensão: NBR 5410**. Rio de Janeiro. 2004, p. 209.

ABNT. **Plugues e tomadas para uso doméstico e análogo: NBR 14136**. Rio de Janeiro, 2012. p. 23.

ABNT. **Sistemas fotovoltaicos conectados à rede — Requisitos mínimos para documentação, ensaios de comissionamento, inspeção e avaliação de desempenho: NBR 16274**. Rio de Janeiro, 2014. p. 52. 

ABNT. **Instalações elétricas de arranjos fotovoltaicos – Requisitos de projeto: NBR 16690**. Rio de Janeiro, 2019. p. 65.

BRASIL. Ministério do Trabalho e Emprego. **NR 10 - Segurança em instalações e serviços em eletricidade**. Brasília, 1978.

INTELLIGENCE, M. Solar tracker Market - share, size, industry report & manufacturers, 2023. Disponível em: <https://www.mordorintelligence.com/industry-reports/solar-tracker-market>. Acesso em: 22 abr. 2024.

ISO. **ISO/IEC 27010:2023: Systems and software engineering - Systems and software Quality Requirements and Evaluation (SQuaRE)**, 2023. 

ISO. **ISO 9060 - Solar energy - Specification and classification of instruments for measuring hemispherical solar and direct solar radiation**. \[S.l.\]. 2018.

SOLAR Tracker Market Size, Share & Industry Analysis, 2023. Disponível em: <https://www.fortunebusinessinsights.com/industry-reports/solar-tracker-market-100448>. Acesso em: 20 abr. 2024.


Tausworthe, Robert C. **"The work breakdown structure in software project management."** Journal of Systems and Software 1 (1979): 181-186.