
# **Apêndice 07 - Memorial de decisões de desenvolvimento de software**

Este apêndice pode ser utilizado para detalhar os cálculos, simulações,
etc. relacionados ao projeto de elementos dos subsistema. Deve-se
registrar as principais decisões realizadas durante a realização do
projeto, particularmente as relativas à escolha de tecnologias para o
desenvolvimento do software, arquiteturas do software, inovações ou
arquitetura da informação.

Deverá ser registrado o histórico das decisões tomadas, um tópico para
cada decisão

Ex:

01.01.2022 \--\> (inovação) Decidiu-se por utilizar redes neurais
artificiais como proposta de inovação para que se pudesse, a partir dos
dados coletados pelo projeto, identificar padrões e favorecer decisões
quanto a \.... (detalhes específicos do projeto)

\...

\...

02.02.2022 -\> (inovação) Decidiu-se desistir da utilização de redes
neurais artificiais. Optou-se por substituí-la por IOT (Internet das
Coisas), a partir da integração com alunos da Engenharia Eletrônica.


# **Apêndice 08 - Plano de testes funcionais do produto**

Esta seção apresenta o plano de testes do produto.

# **Apêndice 09 - Manual de montagem e uso do produto**

Esta seção apresenta o manual de uso do produto. De forma opcional, este
manual pode ser entregue em arquivo separado.

# **Apêndice 10 - Manual de manutenção do produto**

Esta seção apresenta o manual de uso do produto. De forma opcional, este
manual pode ser entregue em arquivo separado.

# **Apêndice 11 - Testes e Manual de instalação do software**

Esta seção apresenta a estratégia realizada para efetivação dos testes
de software(unitário, integração, funcional, aceitação, desempenho,
carga, vulnerabilidade ou outros) manual de instalação e uso dos
produtos de software. De forma opcional, o manual de uso pode ser
entregue em arquivo separado.

# **Apêndice 12 - Autoavaliação dos integrantes**

Em todos os pontos de controle, todos os integrantes de um grupo devem
realizar uma autoavaliação e anexá-la ao relatório entregue pelo grupo.
Esta autoavaliação consistirá em uma tabela com os nomes dos alunos do
grupo e uma descrição de como cada um deles contribuiu individualmente
para o projeto.

