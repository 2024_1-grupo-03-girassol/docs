
# **Integração de subsistemas e finalização do produto**

A seção 04 "**Integração de subsistemas e finalização do produto**" se
refere à fase 04 do ciclo de vida do projeto e tem por objetivo detalhar
a integração final dos subsistemas e a finalização do projeto de um
protótipo de produto. Dentre os aspectos a ser abordado nesta seção,
destaca-se:

-   Desenvolvimento de diagramas de integração do produto;

-   Detalhamentos de ações para integração entre subsistemas

# **Diagrama de integração**

Elaborar o diagrama de integração entre subsistemas, conforme modelo
apresentado [no]{.underline} Plano de Ensino.

# **Detalhamento de atividades para integração entre subsistema 01 e 02**

Descrever atividades e apresentar documentação técnica referente a
integração entre os subsistemas.

# **Detalhamento de atividades para integração entre subsistema 01 e 03**

Descrever atividades e apresentar documentação técnica referente a
integração entre os subsistemas.

# **Detalhamento de atividades para integração entre subsistema 02 e 03**

Descrever atividades e apresentar documentação técnica referente a
integração entre os subsistemas.
