**Universidade de Brasília - UnB**

**Faculdade UnB Gama - FGA**

**Grupo 03 - Tracker Solar 🌻**

**Brasília, DF**

**2024**


**Sumário**

[**1.** **Introdução** [4](#introdução)](introducao/#)

[**1.1.** **Detalhamento do problema**
[4](#detalhamento-do-problema)](introducao/#detalhamento-do-problema)

[**1.2.** **Levantamento de normas técnicas relacionadas ao problema**
[4](#levantamento-de-normas-técnicas-relacionadas-ao-problema)](introducao/#12-levantamento-de-normas-tecnicas-relacionadas-ao-problema)

[**1.3.** **Identificação de solução comerciais**
[4](#identificação-de-solução-comerciais)](introducao/#13-identificacao-de-solucoes-comerciais)

[**1.4.** **Objetivo geral do projeto**
[4](#objetivo-geral-do-projeto)](introducao/#14-objetivo-geral-do-projeto)

[**1.5.** **Objetivo específicos do projeto**
[5](#objetivo-específicos-do-projeto)](introducao/#15-objetivos-especificos-do-projeto-em-cada-area)

[**2.** **Concepção e detalhamento da solução**
[6](#concepção-e-detalhamento-da-solução)](requisitos-arquitetura/#concepção-e-detalhamento-da-solução)

[**2.1.** **Requisitos gerais**
[6](#requisitos-gerais)](requisitos-arquitetura/#requisitos-gerais)

[**2.2.** **Arquitetura geral da solução**
[6](#arquitetura-geral-da-solução)](arquitetura.md)

[**2.2.1.** **Arquitetura do subsistema 01**
[7](#arquitetura-do-subsistema-01)](arquiteturaS01.md)

[**2.2.2.** **Arquitetura do subsistema 02**
[7](#arquitetura-do-subsistema-02)](arquiteturaS02.md)

[**2.2.3.** **Arquitetura do subsistema 03**
[7](#arquitetura-do-subsistema-03)](arquiteturaS03.md)

[**2.2.4.** **Arquitetura do Software**
[7](#arquitetura-do-software)](arquiteturaS04.md)

[**3.** **Fase 3: Projeto e construção de subsistemas da solução
proposta**
[8](#fase-3-projeto-e-construção-de-subsistemas-da-solução-proposta)](prova-de-conceito/#fase-3-projeto-e-construção-de-subsistemas-da-solução-proposta)

[**3.1.** **Projeto do subsistema 01**
[8](#projeto-do-subsistema-01)](prova-de-conceito/#projeto-do-subsistema-01)

[**3.1.1.** **Projeto do elemento 01**
[8](#projeto-do-elemento-01)](prova-de-conceito/#projeto-do-elemento-01)

[**3.1.2.** **Projeto do elemento 02**
[8](#projeto-do-elemento-02)](prova-de-conceito/#projeto-do-elemento-02)

[**3.2.** **Projeto do subsistema 02**
[8](#projeto-do-subsistema-02)](prova-de-conceito/#projeto-do-subsistema-02)

[**3.2.1.** **Projeto do elemento 01**
[9](#projeto-do-elemento-01-1)](prova-de-conceito/#projeto-do-elemento-01-1)

[**3.2.2.** **Projeto do elemento 02**
[9](#projeto-do-elemento-02-1)](prova-de-conceito/#projeto-do-elemento-02-1)

[**3.3.** **Projeto do subsistema 03**
[9](#projeto-do-subsistema-03)](prova-de-conceito/#projeto-do-subsistema-03)

[**3.3.1.** **Projeto do elemento 01**
[9](#projeto-do-elemento-01-2)](prova-de-conceito/#projeto-do-elemento-01-2)

[**3.3.2.** **Projeto do elemento 02**
[9](#projeto-do-elemento-02-2)](prova-de-conceito/#projeto-do-elemento-02-2)

[**4.** **Integração de subsistemas e finalização do produto**
[10](#integração-de-subsistemas-e-finalização-do-produto)](integracao/#integração-de-subsistemas-e-finalização-do-produto)

[**4.1.** **Diagrama de integração**
[10](#diagrama-de-integração)](integracao/#diagrama-de-integração)

[**4.2.** **Detalhamento de atividades para integração entre subsistema
01 e 02**
[10](#detalhamento-de-atividades-para-integração-entre-subsistema-01-e-02)](integracao/#detalhamento-de-atividades-para-integração-entre-subsistema-01-e-02)

[**4.3.** **Detalhamento de atividades para integração entre subsistema
01 e 03**
[10](#detalhamento-de-atividades-para-integração-entre-subsistema-01-e-03)](integracao/#detalhamento-de-atividades-para-integração-entre-subsistema-01-e-03)

[**4.4.** **Detalhamento de atividades para integração entre subsistema
02 e 03**
[10](#detalhamento-de-atividades-para-integração-entre-subsistema-02-e-03)](integracao/#detalhamento-de-atividades-para-integração-entre-subsistema-02-e-03)

[**5.** **Apêndice 01 -- Aspectos de gerenciamento do projeto**
[11](#apêndice-01-aspectos-de-gerenciamento-do-projeto)](integracao/#apêndice-01-aspectos-de-gerenciamento-do-projeto)

[**5.1.** **Termo de abertura do projeto**
[11](#termo-de-abertura-do-projeto)](apendice1/#termo-de-abertura-do-projeto)

[**5.2.** **Lista É / Não É** [11](#lista-é-não-é)](apendice1/#lista-é-não-é)

[**5.3.** **Organização da equipe**
[11](#organização-da-equipe)](apendice1/#organização-da-equipe)

[**5.4.** **Repositórios** [12](#repositórios)](apendice1/#repositórios)

[**5.5.** **EAP (Estrutura Analítica de Projeto) Geral do Projeto**
[12](#eap-estrutura-analítica-de-projeto-geral-do-projeto)](apendice1/#eap-estrutura-analítica-de-projeto-geral-do-projeto)

[**5.5.1.** **EAP do subsistema 01**
[12](#eap-do-subsistema-01)](apendice1/#eap-do-subsistema-01)

[**5.5.2.** **EAP do subsistema 02**
[12](#eap-do-subsistema-02)](apendice1/#eap-do-subsistema-02)

[**5.6.** **Definição de atividades e cronograma de execução**
[12](#definição-de-atividades-e-cronograma-de-execução)](apendice1/#definição-de-atividades-e-cronograma-de-execução)

[**5.7.** **Levantamento de riscos**
[12](#levantamento-de-riscos)](apendice1/#levantamento-de-riscos)

[**5.8.** **Orçamento estimativo**
[13](#orçamento-estimativo)](#orçamento-estimativo)

[**1.** **Apêndice 02 -- Desenhos Técnicos mecânicos**
[14](#apêndice-02-desenhos-técnicos-mecânicos)](apendice2/#apêndice-02-desenhos-técnicos-mecânicos)

[**2.** **Apêndice 03 -- Diagramas elétricos e eletrônicos**
[15](#apêndice-03-diagramas-elétricos-e-eletrônicos)](apendice3/#apêndice-03-diagramas-elétricos-e-eletrônicos)

[**3.** **Apêndice 04 -- Diagramas de sistemas térmicos e/ou
hidráulicos**
[16](#apêndice-04-diagramas-de-sistemas-térmicos-eou-hidráulicos)](apendice4/#apêndice-04-diagramas-de-sistemas-térmicos-eou-hidráulicos)

[**4.** **Apêndice 05 -- Documentação de software**
[17](#apêndice-05-documentação-de-software)](apendice5/#apêndice-05-documentação-de-software)

[**5.** **Apêndice 06 -- Memorial de cálculo de elementos do projeto**
[18](#apêndice-06-memorial-de-cálculo-de-elementos-do-projeto)](apendice6/#apêndice-06-memorial-de-cálculo-de-elementos-do-projeto)

[**5.1.** **Detalhar o projeto do elemento 01 do subsistema 01.**
[18](#detalhar-o-projeto-do-elemento-01-do-subsistema-01.)](apendice6/#detalhar-o-projeto-do-elemento-01-do-subsistema-01.)

[**5.2.** **Detalhar o projeto do elemento 02 do subsistema 01.**
[18](#detalhar-o-projeto-do-elemento-02-do-subsistema-01.)](apendice6/#detalhar-o-projeto-do-elemento-02-do-subsistema-01.)

[**6.** **Apêndice 07 -- Memorial de decisões de desenvolvimento de
software**
[19](#apêndice-07-memorial-de-decisões-de-desenvolvimento-de-software)](apendice6/#apêndice-07-memorial-de-decisões-de-desenvolvimento-de-software)

[**7.** **Apêndice 08 -- Plano de testes funcionais do produto**
[20](#apêndice-08-plano-de-testes-funcionais-do-produto)](#apêndice-08-plano-de-testes-funcionais-do-produto)

[**8.** **Apêndice 09 -- Manual de montagem e uso do produto**
[21](#apêndice-09-manual-de-montagem-e-uso-do-produto)](#apêndice-09-manual-de-montagem-e-uso-do-produto)

[**9.** **Apêndice 10 -- Manual de manutenção do produto**
[22](#apêndice-10-manual-de-manutenção-do-produto)](#apêndice-10-manual-de-manutenção-do-produto)

[**10.** **Apêndice 11 -- Testes e Manual de instalação do software**
[23](#apêndice-11-testes-e-manual-de-instalação-do-software)](#apêndice-11-testes-e-manual-de-instalação-do-software)

[**11.** **Apêndice 12 -- Autoavaliação dos integrantes**
[24](#apêndice-12-autoavaliação-dos-integrantes)](#apêndice-12-autoavaliação-dos-integrantes)

[**12.** **Apêndice 13 -- Atas de reuniões**
[25]()](atas)

[**12.** **Referências Bibliográficas**
[26](#referências-bibliográficas)](bib/#referências-bibliográficas)

[**13.** **Anexo 01 -- Catálogo de componentes utilizados no projeto**
[27](#anexo-01-catálogo-de-componentes-utilizados-no-projeto)](anexo/#anexo-01-catálogo-de-componentes-utilizados-no-projeto)





