# **Concepção e detalhamento da solução**

A seção 02 "Concepção e detalhamento da solução" se refere à fase 02 do
ciclo de vida do projeto e tem por objetivo apresentar a arquitetura da
solução/sistema para o problema.

# **Requisitos Gerais**

## **Método de Elicitação de Requisitos**

Os requisitos do sistema foram estabelecidos por meio de sessões de _brainstorming_, nas quais os membros da equipe se reuniram para debater e definir coletivamente os requisitos necessários. Durante essas reuniões, foram discutidas diferentes ideias, buscando promover a integração das visões e expectativas de todos os envolvidos no projeto, independentemente da área de atuação.

O _brainstorming_ é uma técnica que busca gerar uma variedade de ideias em um curto período de tempo. Segundo o artigo sobre _brainstorming_ do [MIT](https://hr.mit.edu/learning-topics/meetings/articles/brainstorming), essa técnica permite que as equipes explorem soluções criativas, evitando julgamentos prematuros e encorajando a colaboração.

## **Priorização de Requisitos usando MOSCOW**

Os requisitos foram priorizados de acordo com o método MOSCOW, que classifica os requisitos em quatro categorias principais:

- **_Must have_ (Deve ter)**: Requisitos essenciais para a entrega do produto. Sem eles, o produto não atenderá às necessidades básicas dos usuários.
- **_Should have_ (Deveria ter)**: Requisitos importantes, porém não essenciais para a entrega inicial do produto. Eles podem ser adiados para futuras iterações do desenvolvimento.
- **_Could have_ (Poderia ter)**: Requisitos que seriam bons de ter se houver tempo e recursos disponíveis, mas podem ser deixados de lado se necessário.
- **_Won't have_ (Não terá)**: Requisitos identificados como fora do escopo da entrega atual do produto. Eles podem ser considerados em futuros projetos ou iterações.

### Tabela 1: Requisitos funcionais para o produto

| ID   | Requisito                   | Descrição                                                                                                                                        | MOSCOW        |
| ---- | --------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ | ------------- |
| RF01 | Controle de Acesso          | O sistema deve permitir o acesso apenas a usuários autorizados.                                                                                  | _Must have_   |
| RF02 | Notificação ao Usuário      | O sistema deve enviar uma notificação ao usuário quando a produção de energia estiver abaixo do esperado ou se houver algum problema no sistema. | _Must have_   |
| RF03 | Histórico de Produção       | O sistema deve armazenar o histórico de produção de energia.                                                                                     | _Should have_ |
| RF04 | Dashboard de Produção       | O sistema deve informar a quantidade de energia gerada.                                                                                          | _Should have_ |
| RF05 | Dashboard de Performance    | O sistema deve informar a quantidade de energia produzida em relação ao esperado.                                                                | _Should have_ |
| RF06 | Suporte de Carga            | O sistema deve ser capaz de suportar uma carga de 30 kg.                                                                                         | _Must have_   |
| RF07 | Movimentação do Motor       | O motor precisa movimentar 150 graus.                                                                                                            | _Must have_   |
| RF08 | Movimentação da Placa       | A placa deve ser capaz de movimentar-se em 5 posições.                                                                                           | _Must have_   |
| RF09 | Torque do Motor             | O motor precisa ser compatível com um torque de 1.4 Nm.                                                                                          | _Must have_   |
| RF10 | Modo de Baixo Consumo       | O microcontrolador deve entrar em modo de baixo consumo de energia durante a noite.                                                              | _Should have_ |
| RF11 | Monitoramento de Produção   | O sistema deve monitorar a produção de energia em tempo real.                                                                                    | _Must have_   |
| RF12 | Dashboard de Consumo        | O sistema deve informar a quantidade de energia consumida.                                                                                       | _Could have_  |
| RF13 | Movimentação da Placa Solar | O sistema deve ser capaz de movimentar a placa solar para a posição ideal.                                                                       | _Must have_   |
| RF14 | Dados do Dashboard          | O sistema deve informar todos os dados utilizados para a movimentação da placa solar.                                                            | _Could have_  |
| RF15 | Aplicativo                  | O sistema utilizará um aplicativo para interação com a placa.                                                                                    | _Won't have_  |
| RF16 | Notificação de Dados        | O sistema deve enviar notificações com todos os dados coletados.                                                                                 | _Won't have_  |

### Tabela 2: Requisitos não funcionais para o produto

| ID    | Requisito      | Descrição                                                                                                                                                 | MOSCOW      |
| ----- | -------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| RNF01 | Usabilidade    | O sistema deve ser intuitivo e fácil de usar, proporcionando uma experiência fluida para os usuários.                                                     | _Must have_ |
| RNF02 | Confiabilidade | O sistema deve ser robusto, funcionando consistentemente com uma baixa taxa de falhas e fornecendo resultados precisos e consistentes.                    | _Must have_ |
| RNF03 | Desempenho     | O sistema deve oferecer um tempo de resposta rápido e uma alta capacidade de processamento para suportar operações intensas sem comprometer a eficiência. | _Must have_ |
| RNF04 | Segurança      | O sistema deve proteger os dados dos usuários e as operações contra ameaças, implementando medidas de segurança como criptografia e controles de acesso.  | _Must have_ |
