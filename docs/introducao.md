# Introdução
O "*Tracker* solar", é uma placa solar fotovoltaica com sistema de rastreamento que acompanha o movimento do sol. Foi pensado com o intuito de aumentar a eficiência da geração de energia, a ser desenvolvido com conhecimentos multidisciplinares, das Engenharias Aeroespacial, Eletrônica, de Energia e de Software.

## 1.1. Detalhamento do problema

A crescente inserção de fontes de energia renováveis nas matrizes energéticas, impulsionada pela necessidade de mitigação de impactos ambientais e climáticos, caminha juntamente com o desenvolvimento de novas tecnologias e inovações.

Nesse contexto, a energia solar fotovoltaica é uma das fontes de energia renovável que vem sendo explorada e cada vez mais utilizada. Esta tecnologia transforma a luz solar em eletricidade por meio de células fotovoltaicas, oferecendo uma fonte de energia abundante e de baixa emissão de carbono. Apesar disto, um dos desafios enfrentados neste tipo de tecnologia é a sua baixa eficiência.

Surge, então, a ideia do "*Tracker* solar", um dispositivo mecânico e eletrônico projetado para orientar as placas solares fotovoltaicas, através do rastreamento do movimento do sol durante o dia.

O "*Tracker* solar" contará com:

- Estrutura metálica - Garantindo a sustentação da placa solar fotovoltaica e permitindo que ela rotacione em torno de um eixo paralelo ao chão.
- Motor de passo - Permitindo o posicionamento preciso da placa solar fotovoltaica através da rotação em torno do eixo.
- Sensores de luminosidade - Para a determinação da posição do sol no céu, convertendo a luz em sinais elétricos proporcionais à luz incidente, que, posteriormente, são processados para detectar a direção do sol.
- Sensores de posição - Permitindo o acompanhamento em tempo real da posição do conjunto de placas e do possível deslocamento delas.
- Sistema de controle - Para receber os sinais enviados pelos sensores de luminosidade e determinar os movimentos necessários para manter a placa solar fotovoltaica alinhada com a trajetória do sol.
- Integração de APIs - O software terá integração com APIs, fornecendo dados da estação solarimétrica em tempo real.
- Software de monitoramento - Um painel virtual que mostrará gráficos com os dados necessários para fazer o monitoramento do "*Tracker* solar". Fazendo o monitoramento em tempo real e fornecendo um histórico dos dados de desempenho do sistema.
- Comunicação remota - O software terá a capacidade de comunicação remota, garantindo o controle e monitoramento do sistema a partir de qualquer localização.
- Fluxo de exceção - Emitindo notificações de alerta para o usuário sempre que ocorrer um problema específico ou potencial problema.
- Fonte de energia - Para manter todo o sistema do *Tracker* solar em pleno funcionamento, o mesmo contará uma fonte de alimentação. Sendo um sistema “On-Grid”, será alimentado pela própria rede elétrica já instalada no local.

## 1.2. Levantamento de normas técnicas relacionadas ao problema

Para o desenvolvimento do projeto, houve um levantamento de normas técnicas, de todas as áreas de conhecimento, para embasamento e a correta execução para as etapas do projeto, tanto para a concepção e planejamento quanto para a execução e avaliação do projeto. A normas possibilitam a padronização testada e garante segurança, eficácia e eficiência das ações.

As seguintes normas foram averiguadas:

- **ABNT NBR 5410** - Instalações elétricas de baixa tensão: Estabelece parâmetros para instalações elétricas de baixa tensão, objetivando a
  segurança de pessoas e animais, o funcionamento da instalação e conservação de bens (ABNT, 2004, p. 1).
  Útil para garantir tanto a segurança de alunos, professores e demais pessoas que interajam com o sistema de *tracker* solar quanto para garantir
  seu correto funcionamento relativo às instalações elétricas.
- **ABNT NBR 14136** - Plugues e tomadas para uso doméstico e análogo: Fixa as dimensões de plugues e tomadas de características nominais
  até 20A / 250V em corrente alternada, para uso doméstico com ligação a sistemas de distribuição com tensões compreendidas entre 100 V
  e 250 V em corrente alternadas (ABNT, 2012, p. 1). Utilizada no projeto para o planejamento e montagem do circuito alimentado pela placa solar fotovoltaica.
- **ISO 9060** - Energia solar: Especificação e classificação de instrumentos para medição solar hemisférica e radiação solar direta: Estabelece uma categorização
  e descrição de dispositivos para medição da radiação solar hemisférica e direta, integradas dentro de uma faixa espectral que varia de 0,3 μm a 4 μm. A classificação
  dos instrumentos é baseada nos resultados obtidos a partir de testes de desempenho (ISO, 2018). O padrão foi utilizado para auxiliar na aferição de dados
  solarimétricos através de instrumentos como o piranômetro.
- **ABNT NBR 16274** - Sistemas Fotovoltaicos Conectados à Rede de distribuição: define os requisitos mínimos de informação e documentação a serem registrados após a
  conclusão da instalação de um sistema fotovoltaico conectado à rede. Além disso, aborda a descrição da documentação, os testes de comissionamento e os critérios de
  inspeção para garantir a segurança e o funcionamento adequado do sistema instalado (ABNT, 2014). Serve para o projeto para avaliação do funcionamento do sistema fotovoltaico
  e para que se possa inspecioná-lo.
- **ABNT NBR 16690:2019** – Instalações elétricas de arranjos fotovoltaicos – Requisitos de projeto: Estabelece os critérios de projeto para as instalações elétricas
  de arranjos fotovoltaicos, abrangendo aspectos como condutores, dispositivos de proteção elétrica, dispositivos de manobra, aterramento e equipotencialização. O
  escopo desta norma abrange todas as partes do arranjo fotovoltaico, exceto os dispositivos de armazenamento de energia, unidades de condicionamento de potência
  ou as cargas(ABNT, 2019).
- **NR 10** - Segurança em instalações e serviços em eletricidade: estabelece os requisitos e condições mínimas objetivando a implementação de medidas
  de controle e sistemas preventivos, de forma a garantir a segurança e a saúde dos trabalhadores que, direta ou indiretamente, interajam em instalações elétricas
  e serviços com eletricidade (BRASIL, 1978).
- **ISO/IEC 25010:2023** - Sistemas e Engenharia de *Software* - Requisitos de Qualidade e Avaliação (SQuaRE): estabelece um modelo de qualidade para produtos de
  tecnologia da informação e comunicação (TIC) e produtos de software, composto por nove características e suas subcategorias, que servem como referência para
  especificação, medição e avaliação da qualidade. Ele pode ser utilizado por diversos interessados, como desenvolvedores, compradores e avaliadores
  independentes, ao longo do ciclo de vida do produto (ISO, 2023). É útil para determinação, avaliação e manutenção da qualidade dos produtos de Software desenvolvidos no
  projeto e determinação e gerenciamento dos requisitos funcionais e não funcionais da solução de *software*.
- **ISO/IEC/IEEE 29148:2018** - Sistemas e Engenharia de *Software* — Processos de ciclo de vida — Engenharia de requisitos:
- **ISO/IEC 27001:2022** - Segurança da informação, cibersegurança e proteção de privacidade - Sistemas de gerenciamento de segurança da informação:
- **ABNT NBR 8800:2008** - Projeto de estruturas de aço e de estruturas mistas de aço e concreto de edifícios: Estabelece os requisitos básicos que que devem ser obedecidos no projeto à temperatura ambiente de estruturas de aço e de estruturas mistas de aço e concreto de edifícios (ABNT, 2008, p. 1). Essencial para a concepção da estrutura.
- **ABNT NBR 12** - Estabelece a aplicação de máquinas e dispositivos caracterizados por proteções fixas, móveis e dispositivos de segurança interligados ou não, que garantem a saúde e segurança.


## 1.3. Identificação de soluções comerciais

O mercado de *Trackers* solares cresceu nos últimos anos, gerando um valor de US$ 7,88 bilhões em 2023, projetando-se para atingir US$ 25,24 bilhões até 2032 (SOLAR, 2023).

A longo prazo, o mercado de *Trackers* deve ter uma demanda maior com a crescente instalação de plantas de energia solar fotovoltaica, já que os sistemas atuais aumentam a capacidade de geração da placa solar fotovoltaica para valores entre 20% - 30% (INTELLIGENCE, 2023).

No mercado global, a região da América do Norte tem a maior parte do mercado e da demanda, com os Estados Unidos liderando o mercado global (SOLAR, 2023).

Durante a fase inicial do projeto foi feita uma pesquisa de mercado de produtos já existentes, a fim de definir nossos requisitos e entender melhor o seu funcionamento. Foram priorizadas para a pesquisa, soluções que possuíssem especificações técnicas bem definidas.

NexTracker Inc., Array Technologies Inc., PV Hardware Solutions S.L.U., Arctech Solar Holding Co. Ltd e Soltec Power Holdings SA são alguns dos principais participantes do mercado (INTELLIGENCE, 2023).

A tabela a seguir faz uma breve comparação entre as soluções comerciais já existentes (duas dos Estados Unidos e uma solução chinesa). Os graus de liberdade podem ser um, para um único eixo, ou dois para seguidores com 2 eixos.


|Nome|Empresa|Angulação|Material|Graus de liberdade|Algoritmo|
|---|---|---|---|---|---|
|NX Horizon[^1]|NextTracker Inc.|$$ \pm 60° $$|Aço galvanizado|1 grau|Algoritmo astronômico com padrão de backtracking|
|Utility Dual Track[^2]|Sun Action Trackers|Rotação: 135° Azimut, Vertical: 60°|Aço galvanizado|2|Sensor em tempo real|
|Vanguard - 2P[^3]|TrinaTracker|110°|Aço de alto rendimento|1|*Smart Tracking Algorithm* que utiliza dados em tempo real, imagens topográficas capturadas por drones e modelagem do terreno.|

**Tabela 1 -** Detalhamento de produtos existentes no mercado.

Fonte: (Autores).


Boa parte das soluções se aplica para plantas de grande porte, com eixos que comportam múltiplas placas solares fotovoltaicas, diferentemente da solução que é apresentada pelo presente trabalho, que visa o controle de uma única placa solar fotovoltaica.


[^1]: Mais informações: [https://www.nextracker.com/nx-horizon-solar-tracker](https://www.nextracker.com/nx-horizon-solar-tracker)

[^2]: Mais informações: [https://sat-energy.com/recources/](https://sat-energy.com/recources/)

[^3]: Mais informações: [https://www.trinasolar.com/en-glb/trinatracker/vanguard](https://www.trinasolar.com/en-glb/trinatracker/vanguard)




## 1.4. Objetivo geral do projeto
O projeto tem como principal objetivo maximizar o potencial da energia solar fotovoltaica, a partir do desenvolvimento de um produto que otimize a captação da radiação solar, aumentando a eficiência da geração de energia elétrica.

## 1.5. Objetivos específicos do projeto

- Construir um produto em que a produção de energia elétrica seja mais eficiente do que em uma placa solar fotovoltaica fixo;
- Desenvolver uma estrutura física que atenda aos padrões de segurança;
- Implementar um sistema de controle que consiga efetuar os movimentos corretos das placas solares fotovoltaicas de acordo com os dados solarimétricos coletados pelos sensores internos e externos;
- Integrar sensores de luminosidade eficazes e confiáveis;
- Desenvolver algoritmos que garantam o posicionamento preciso da placa solar fotovoltaica ao longo do dia;
- Garantir o pleno funcionamento de todo o sistema com uma fonte de alimentação segura;
- Criar um *software* que possibilite a configuração remota do equipamento pela internet;
- Desenvolver uma aplicação *web* com uma interface amigável de visualização de dados e de controle do "*Tracker* solar" em tempo real;