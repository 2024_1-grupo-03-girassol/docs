
# **Apêndice 02 - Desenhos Técnicos mecânicos**

Desenhos técnicos mecânicos: indicação de cotas, cortes (se necessário),
elementos de fixação, simbologia de soldagem (se necessário), lista de
elementos e materiais em desenhos de conjunto, nas legendas indicação do
tipo de material, unidade, massa do elemento, escala, diedro, projetista
ou desenhista, revisor, dentre outras informações que auxiliem na
fabricação das estruturas mecânicas do sistema. Vide item 7 do apêndice
03;
