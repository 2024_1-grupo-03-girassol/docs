# **Atas de reuniões**
Aqui constam todas as atas das reuniões realizadas no horário de aula de extraclasse.


## Reunião 01 - 27/03/2024
Data: 27/03/2024

Horário: 17h30 às 18h10

Local: Sala S1

Presentes: Adrian, Karine, Prof. Alex, João Gabriel, João Paulo, Joaquim, Marina, Hugo, Jorge, Daniel


### Objetivos

### Deliberações
A reunião foi aberta pelo líder da equipe, Ingryd.
Discussão sobre o projeto Girassol Solar.
Foi discutido o projeto do Girassol Solar, atribuído ao grupo após desafios iniciais.
Foi decidido que a equipe será dividida nas áreas de Estrutura, Energia/Eletrônica e Software.
O grupo foi incentivado a pesquisar e contribuir com ideias para o projeto, para a próxima reunião, no dia 29/03/2024.
Todos os membros foram incentivados a realizar pesquisas prévias sobre suas áreas designadas até a próxima reunião, agendada para 29/03/2024.
O Professor Alex se comprometeu a fornecer suporte, especialmente em relação aos desafios da estrutura. Foi discutida a possibilidade de coletar dados de API's públicas e localmente para cálculos em tempo real.

## Reunião 02 - 29/03/2024
Data: 29/03/2024

Hora: 20h - 21h

Local: Discord

Presentes: Adrian, Camila, Hugo, Joaquim, Jorge, João Paulo, Karine, Daniel, Marina e Pedro.


### Deliberações
Contribuições de cada integrante do grupo, após pesquisas sobre o tema:

Adrian: Em relação aos APIs, não conseguiu acessar os links do INPE, tentou
utilizar o da NASA mas deu erro. Link será compartilhado para outros tentarem utilizar. O
repositório estava com os links errados.

Camila: Sem contribuições, pois estava doente.

Hugo: Concluiu a partir das pesquisas que não compensaria fazer um *tracker* de
dois eixos. Clonou o template do github da professora. Acredita que seria melhor utilizar um
sensor ao invés de dados de API.

Joaquim: Encontrou alguns artigos da parte de energia, sem conteúdo da parte
eletrônica. Encontrou conteúdos relacionados aos cálculos que serão utilizados no projeto.

Jorge: Pesquisou sobre seguidor solar, vantagens e desvantagens de seu uso.
Encontrou produtos prontos, para ter como base para o entendimento do projeto.
Compartilhou um vídeo da produção de um seguidor solar.

João Paulo: Possui uma Orange PI. Pesquisou sobre a possibilidade de utilizá-la
no projeto.

Karine: Possui uma mini placa solar fotovoltaica, que pode ser utilizada para testes. Sobre o
*tracker*, viu que muitas fazendas utilizam para aumentar a eficiência energética e acredita
ser possível fazer.

Daniel: Acredita que seria melhor utilizar um sensor para a coleta de dados,ter um
sistema integrado para conseguir monitorar esses dados e conseguir, se possível, ter
controle da posição da placa através de um aplicativo. Sugeriu que os docentes de software
fiquem responsáveis pelos arquivos no github.

Marina: Encontrou artigos e TCCs sobre o tema, vai filtrar esse conteúdo e enviar
para o grupo. Acredita que seria um diferencial o usuário poder alterar a posição da placa,
conforme ideia do Daniel.

Pedro: Acredita que a parte estrutural será a mais complicada devido a quantidade
de elementos. Sugeriu a utilização de um rastreador de apenas um eixo.

Divisão por categoria:
**Coordenador(a) Geral:** Karine

**Diretor(a) de Qualidade:** Joaquim

**Diretor(a) Técnico(a) Estruturas:** Camila

**Diretor(a) Técnico(a) Energia e Eletrônica:** Jorge Guilherme

**Diretor(a) Técnico(a) Software:** João Paulo

**Desenvolvedores(as):** Demais integrantes

Comentários importantes:
Ainda existe vaga no grupo, então quem souber de alguém que esteja na disciplina e
ainda estiver sem grupo, convidar para participar do grupo.
Manter sempre a comunicação entre os integrantes!
Será criado uma comunidade no WhatsApp com um grupo para transmissão das
informações importantes e outro para a comunicação entre os integrantes.

## Reunião 03 - 03/04/2023
Data: 03/04/2024

Horário: 16h - 18h

Local: Sala S1

Presentes: Adrian, Camila, Hugo, Joaquim, Jorge, João Paulo, Karine, Daniel, Marina e Pedro

Ata feita por: Adrian Soares e Jorge Guilherme

### Objetivos

### Deliberações
Em seguida, os alunos se reuniram. Em resumo:

Todos concordaram com o entendimento do projeto.

Aqueles que ainda têm dúvidas sobre o projeto devem ler os artigos e links enviados pelos integrantes Marina e Joaquim no grupo do WhatsApp.

É necessário sentar com os diretores técnicos/gerentes para definir um cronograma das atividades para cada aula. Um cronograma preliminar para duas semanas foi definido, mas ficou acordado que um mais abrangente será definido quando as principais decisões sobre os componentes forem tomadas.

Nas próximas aulas, os subsistemas devem pensar "O que devemos entregar para o primeiro ponto de controle 1".

Pretende-se visitar uma empresa de *trackers* solares de um colega de um dos integrantes do grupo.

Devem ser analisados os gráficos de energia, radiação e eficiência.

É necessário verificar as especificações técnicas da placa solar e do motor disponíveis pelo professor Alex.

Por fim o professor Alex, no laboratório, nos apresentou o possível tipo de placa que será utilizado. É uma placa comercial com por volta de 1x2 m e pesando um pouco mais de 20 kg. Ela é capaz de gerar 550 W de potência.

### Feedback dos professores
Professor Alex: A reunião se iniciou com a discussão dos componentes e materiais necessários para o projeto. Foi ressaltada a importância de apresentar uma visão detalhada do projeto, incluindo desenhos de dimensões, esquemas, componentes, _drivers_, arquitetura e acessórios. Além disso, foram destacados os protótipos de média fidelidade como ferramentas essenciais para avaliações, compreendendo o problema, a arquitetura da solução e o pré-projeto. Todo o planejamento concreto com toda a documentação técnica, não será necessário entregar algo físico pronto.

Também foi enfatizado a necessidade de tomar decisões importantes neste estágio do projeto. Questões como a quantidade de eixos e a possibilidade de aprimorar o que já está em funcionamento foram levantadas. Além disso, foi discutido o tamanho das placas e a estimativa de potência necessária, bem como a seleção adequada do tipo de motor, com uma inclinação provável para motores de passo. Soluções comerciais já existem e precisam ser consultadas.

O professor resumiu as etapas do projeto da seguinte forma:

* Ponto de Controle 1: Problema(propor uma solução, entender o problema, definir a solução, definir o requisito e definir a arquitetura--conceito de energia/eletrônica,conceito estrutura e conceito de software).
  * Materiais que serão usados(Nada físico).
  * Planejamento definido(Visão detalhada do projeto com esquemáticos de cada área).
  * Embasar o projeto em uma Solução Comercial já existentes.
  * Como usar a placa?
* Ponto de Controle 2: Montagem dos subsistemas de forma isolada, sem integração.
* Ponto de Controle 3: Integração dos subsistemas e testes.


Professor Rafael: Na sequência, o professor Rafael contribuiu com a discussão, destacando a importância de avaliar a quantidade de eixos necessários para o projeto. A possibilidade de aprimorar soluções já existentes foi mencionada, desde que seja viável e eficiente. Também foram discutidos detalhes técnicos, como o tamanho das placas e a estimativa de potência necessária, além da seleção adequada do tipo de motor, com uma inclinação provável para motores de passo.

Será preciso estimar a carga e utilizar um motor que aguente a mesma, suporte o arrasto e a força do vento, já que a placa estará ao ar livre. Lembrar que os cálculos devem considerar a inclinação da placa. O professor sugeriu uma estimativa até a aula de sexta-feira para que se possa escolher um motor adequado que tenha torque igual ou maior que o necessário.

A busca por soluções comerciais já utilizadas também foi mencionada para inspiração. Foram abordadas questões relacionadas ao motor de passo, incluindo torque, resolução e considerações de carga, como vento e arrasto. Também foi discutida a necessidade de decidir entre malha aberta ou malha fechada para o sistema eletrônico.

Professora Suélia: A professora Suélia também participou e destacou a importância de focar em poucas variáveis, de forma profunda. Utilizar todas as ferramentas disponíveis para especificar os componentes eletrônicos, ter um bom processamento de sinais e analisar o comportamento da curva do sinal mais importante (irradiação). A discussão sobre a eficiência e a irradiação também foi introduzida.

## Reunião 04 - 05/04/2024
Data: 05/04/2024

Horário: 14h - 18h

Local: Auditório

Presentes: Adrian, Camila, Daniel, Hugo, Joaquim, Jorge, João Paulo, Karine, Marina e Pedro Guimarães

Ata feita por: 


### Objetivos
Aprender a utilizar o GitLab e decisões gerais do projeto

### Deliberações
Gitlab: A próxima etapa foi aprender a usar o repositório Git para salvar os documentos em Markdown.

Foi criado um modelo de ata de reunião e ficou acordado entre os integrantes que todos poderão editar a ata. Ela será postada em um mesmo arquivo, separada por títulos que evidenciam as datas.

A ata já está linkada na página como um apêndice.

Ao final da reunião, alguns membros das equipes de software e eletrônica discutiram sobre microcontroladores e a arquitetura de software que poderá ser utilizada.

Provavelmente será utilizado um RaspberryPi ou OrangePi. Para tanto, será necessário adquirir conversores ADC e DAC para a entrada de dados dos sensores de irradiação e para a saída de controle do motor. Possivelmente será utilizada uma ponte para tensão.

As linguagens de programação utilizadas serão Python e JavaScript/TypeScript.

Será utilizada uma arquitetura de microsserviços para facilitar a independência do desenvolvimento inicial de cada subgrupo de software.

### Feedback dos professores
Rhander e Rafael: Os professores mostraram as instalações no LDTEA e frisaram a importância de colocarmos a mão na massa, com segurança e acompanhamento dos professores.

O professor Rhander mostrou os equipamentos disponíveis, como motores de passo, um atuador, mancais, acopladores, correias e outros que poderão ser usados. Sugeriu a utilização de metalon como material para a estrutura do nosso projeto. O professor também mencionou um contato que fornece materiais de alumínio a preços com desconto para alunos

O professor Rafael frisou a importância da realização dos cálculos e do esboço da estrutura, pois quem chegar primeiro com isso terá acesso aos materiais disponíveis.

Foi separada uma caixa para a nossa equipe. Os materiais não podem sair do laboratório, por enquanto. Serão usados lá e guardados nessa caixa com o nome do grupo, que contém cabos e outros componentes eletrônicos.Os integrante Joaquim e Jorge separaram os materiais para a equipe.

O professor Rhander nos propôs algumas ideias de base para a estrutura, como fazer uma base mais segura e chumbá-la com concreto. Ele nos indicou alguns locais onde podemos encontrar os materiais para a estrutura e também o motor com a caixa de redução. O professor também citou uma ideia de ter um motor central, acompanhando dois mancais que vão junto ao eixo, auxiliando no equilíbrio.

Alex e Suélia: Reunião com os professores:
Os professores conversaram com os alunos. Um dos professores explicou melhor sobre os dados meteorológicos da estação da faculdade, que estão offline.

Ele explicou a possibilidade de utilizá-los como uma série histórica de dados, com aproximadamente 1 ano e meio de informações, para estimar a irradiância da região e rotacionar com precisão de minutos.

Além dessa possibilidade, o professor mencionou parceiros que estavam fornecendo os dados da região coletados em tempo real via satélite. Ele tentará contatá-los depois.

A partir dessas informações, o grupo decidiu que o sistema provavelmente será híbrido, utilizando informações estáticas e em tempo real dos sensores e, se possível, da API via satélite.

A professora Suélia frisou a importância de fazer algo simples que funcione.

É importante sempre se comunicar e conversar sobre as integrações entre as áreas, para que nada tenha que ser implementado antes da hora.

O grupo acordou que, todas as sextas-feiras, cada subgrupo deverá contatar os professores de suas respectivas áreas para que eles possam acompanhar e dar feedback sobre as decisões tomadas.

Em conversa com a equipe de estrutura, o professor Alex destacou a importância de realizar todos os cálculos que garantirão e embasarão a escolha dos materiais. Ele salientou que somente com esses cálculos será possível desenvolver o projeto de forma adequada.

Por fim, o professor Alex sugeriu que usássemos um motor DC (que já está disponível) para a rotação do eixo. O motor possui uma caixa de engrenagem para redução do torque, que provavelmente será suficiente para estabilizar a placa. No entanto, as estimativas deverão ser apresentadas para que possamos escolher o motor ideal.

O motor deverá ter um encoder acoplado para leitura da rotação e controle utilizando microcontrolador.

## Reunião 05 - 10/04/2024Data: 10/04/2024

Horário: 16h-18h

Local: Auditório

Presentes: Adrian, Camila, Daniel, Hugo, Joaquim, Jorge, João Gabriel, Karine, Marina e Pedro Guimarães

Ata feita por: Joaquim e Marina

### Objetivos
Dar andamento ao projeto e fazer uma aula de Gitlab para alinhar conhecimentos

### Deliberações
Adrian: Cronograma + ajuda com o Gitlab

Camila: Desenvolvimento da estrutura e simulação + aula de Gitlab

Daniel: Organograma + ajuda com o Gitlab

Hugo: Auxílio c tomadas de decisões p eletrônica

João Gabriel: Ajuda com o Gitlab

Joaquim: Pesquisas p eletrônica + aula de Gitlab

Jorge: Pesquisas p eletrônica + aula de Gitlab

Karine: Atribuição de tarefas + aula de Gitlab

Marina: Introdução do doc + aula de Gitlab

Pedro Guimarães: Aula de Gitlab

### Considerações finais
Introdução do documento em andamento.

Estruturas e Eletrônica em andamento com as tomadas de decisão

### Feedback dos professores
Suélia - Reunião 12/04 às 14h para verificar OrangePi

Alex - Acelerar no andamento do projeto

## Reunião 06 - 12/04/2024
Data: 12/04/2024

Horário: De 14h até 18h

Local: Auditório e LDTEA

Presentes: Camila, Pedro, Pedro Henrique, João Paulo, Hugo , Adrian, João Gabriel, Joaquim, Ingryd, Jorge

Ata feita por: Joaquim

### Objetivos
Conseguir resolver o problema da API com o professor Alex, utilizando a API que ele tem, ou do piranômetro, ou a API do _open weather_. E vamos nos basear nos dados que precisamos que dados gerais (latitude e longitude), condições climáticas (temperatura, irradiância solar, precipitação e velocidade do vento) e a inclinação da placa, esses são dados que vamos precisar para fazer com que a placa tenha a melhor eficiência possível, como por exemplo a latitude e longitude fornecem informações sobre a localização geográfica do sol, as condições climáticas que são vitais para estimar a quantidade de energia solar disponível em um determinado momento e local, e a inclinação da placa solar para maximizar a eficiência do sistema.

Limpeza da placa, na instalação do sistema operacional com interface gráfica e também na montagem do circuito na _protoboard) para a visualização das ondas no osciloscópio.

Mostrar o modelo 3D do projeto para os professores e solicitar as considerações e sugestões deles a
respeito do mesmo.

### Deliberações
O eixo utilizado na modelagem estava muito espesso o que aumentaria a carga na estrutura. Foi disponibilizado um eixo para uso de 1,5 m de comprimento e 16 mm de diâmetro, no LDTEA. Com isso, foi optado por utilizar a placa no modo retrato para que ela caiba no eixo. A estrutura de apoio para a placa também aumentaria a carga, por isso foram analisadas outras maneiras de fixá-la no eixo. Por fim, foi verificada a estrutura da placa e as posições dos furos para parafuso já existentes nela

### Considerações finais
Decidimos que vamos montar a API e usar os dados do piranômetro como referência e comparar com os da API. O João Paulo vai entrar em contato com o professor Renato para conseguir abstrair os dados do piranômetro com mais facilidade e tentar coloca-lo “on-line”. Iremos ter mais uma reunião antes da próxima aula para discutirmos e fazermos os diagramas que estão faltando.

A Limpeza da placa foi realizada com o auxílio da professora Suelia e do professor temporário Tiago. Diminuiu bastante o aquecimento dela e ela conseguiu funcionar bem durante aula.
O grupo optou pela instalação da interface gráfica para que fosse mais fácil o desenvolvimento dos códigos e do trabalho.
A instalação da interface gráfica foi realizada pelo Jorge, Joaquim e Hugo no laboratório NEI 1.

Com o auxílio do professor Tiago, o grupo conseguiu visualizar as ondas no osciloscópio e verificou que o circuito formado pelo sensor LDR junto com o capacitor se comporta como um filtro-passa baixa. O grupo deve explicar melhor está relação no projeto e associá-lo as características do PWM.

Com relação ao motor, o grupo deve verificar na próxima aula se há  a exigência da construção do zero do drive do motor ou se o grupo pode arranjar um drive construído no mercado. 

A definição do motor ainda não foi concluída.

Com essas reflexões foi possível remodelar a estrutura e verificar que é possível utilizar o motor de passo NEMA 23 no projeto.

### Feedback dos professores
Tiago: acompanhou mais a nossa aula e passou algumas dicas para o projeto, entre elas o uso do motor de passos, pois o grupo conseguirá determinar o ângulo a ser girado pela placa com maior facilidade.

Rafael: O professor propôs o uso de um eixo que está disponível no laboratório no LDTEA, e de um motor de passo NEMA 23. Além disso, ele sugeriu rever a estrutura, retirando o suporte para a placa, diminuindo a espessura do eixo e posicionando a placa em modo retrato.

Alex: O professor, assim como o professor Rafael, sugeriu a utilização da placa em modo retrato e nos levou ao laboratório no LDTEA para conferir as medidas da placa.


## Reunião 07 - 17/04/2024
Data: 17/04/2024

Horário: 16h - 18h

Local: Sala S4 e LDTEA

Presentes: Camila, Ingryd, Marina, Jorge, Adrian, João Paulo, João Gabriel, Daniel, Hugo, Pedro Henrique, Pedro Guimarães.

Ata feita por: Joaquim

### Objetivos
Apresentar o modelo aos professores e considerar as possíveis melhorias e sugestões.

Dar andamento ao projeto e fazer uma aula de Gitlab para alinhar conhecimentos.


### Deliberações
Analisar a presença do atuador na estrutura. Selecionar um novo eixo, adequado a essa aplicação. Adicionar um novo apoio à estrutura, localizada no meio do eixo. Retornar a placa a uma orientação “paisagem”.

Seleção dos dados e gráficos a serem utilizados na página web para atingir os objetivos do trabalho, início da criação da API e do dashboard, participação na reunião de decisão da estrutura e materiais com professores e os demais alunos da S4 e LDTEA.

Foi realizada uma discussão sobre o uso de LDRs como auxiliares à API e aos dados do piranômetro.

### Considerações finais
Apresentar os cálculos estruturais do projeto. Cálculo de torque, cálculo de raio mínimo do eixo, desenho de corpo livre da estrutura, definição do motor. Fazer modificações na estrutura do projeto.

### Feedback dos professores
Ambos os professores sugeriram a utilização do atuador, por ser mais preciso.

O professor Rafael acredita que adicionar um terceiro suporte, no meio da estrutura, fornecerá maior estabilidade. Depende dos cálculos realizados, para saber se o eixo suportará todos os esforços. Sugeriu a utilização de sistemas que irão movimentar o eixo e manter estabilizado, incluindo polias, engrenagens ou correias.

O professor Rhander disponibilizou uma outra alternativa ao eixo sugerido anteriormente, devido ao fato de não ser adequado a essa utilização. O novo eixo é um tubo circular. Além disso, sugeriu um apoio perpendicular às bases da estrutura. Disse ser possível realizar todas as soldagens e cortes no próprio laboratório, podendo ajudar os integrantes do grupo ou então a partir de terceiros. Nesse caso, a pessoa cobraria pelo serviço. Passará o contato de uma empresa para aquisição do aço.

## Reunião 07 - 19/04/2024
Data: 19/04/2024

Horário: 14h até 18h

Local: Auditório, Laboratório NEI (Laboratório de Circuitos) e LDTEA

Presentes: Camila, Ingryd, Marina, Jorge, Adrian, João Paulo, João Gabriel, Daniel, Hugo, Pedro Henrique, Pedro Guimarães

Ata feita por: Joaquim

### Objetivos
Apresentar as alterações aos professores e verificar os materiais disponíveis na oficina.
Definição dos requisitos do projeto.

Atualização da biblioteca do Python no microcontrolador.

Levantamento de requisitos e consumo de dados do piranômetro.

### Deliberações
Definir o motor a ser utilizado e fazer um orçamento dos materiais necessários.

Os requisitos básicos foram discutidos em grupo, de cada um dos subsistemas e que foram registrados no repositório. Os requisitos de software foram definidos, como a exibição de dados de expectativa X realidade e o controle manual do eixo.

Foi feita a extração de dados de exemplo do Data Logger utilizando o software proprietário para geração de relatório em csv.

Verificou-se, também, como o software pode enviar esses dados via comunicação HTTP e TCP.

Também foi realizada a pesquisa para a configuração do roteador para o piranômetro, da marca TP-Link. Só não foi possível sua configuração pois o regulador de tensão estava em curto.

Por fim, houve uma reunião com os membros de eletrônica para verificar a instalação de bibliotecas faltantes na placa OrangePI.

### Considerações finais

### Feedback dos Professores
Os professores Rhander e Rafael, apresentaram os espaços que podem ser utilizados para a construção da estrutura. Apresentaram os materiais que podem ser comprados, como mancais, motor, redutor. E disponibilizaram novos eixos, com diferentes comprimentos, de tubo circular e de tubo quadrado. Sugeriram refazer as simulações e outros cálculos para embasar a utilização do eixo e garantir o seu funcionamento correto, sem rupturas.

Professor Alex:  Mostrou o software de visualização dos dados do micro inversor, como as curvas são feitas e garantiu a possibilidade de exportação dos dados (só não conseguiu mostrar como era feito, na hora). Ademais, disponibilizou alguns dados do Data Logger da forma em que será disponibilizado para a aplicação e mostrou o roteador e seu modelo para posterior configuração.

Professora Carla: Nos alertou para não focar em fazer UI's desnecessárias e sim utilizar soluções prontas e pensar na utilidade para o usuário. Os critérios de qualidade são flexíveis para a disciplina de PI-2 e o mais importante é uma solução que resolva as dores do usuário. Uma sugestão foi a utilização de BI para a construção dos dashboards e a notificação de problemas de comunicação e de desvio da expectativa de geração foi mencionada como prioridade para o software. Uma sugestão foi um sistema de notificação por e-mail, tomando cuidado para não fazer notificações indevidas (como exemplo, notificar baixa geração de energia à noite). A professora também enfatizou que um dos quesitos mais importantes é a análise de exceções da aplicação e de como se recuperar delas.

Professora Suélia não estava presente na aula, mas professor Tiago auxiliou  durante a aula e ficou a disposição para ajudar .Caso precise usar o laboratório NEI nas sextas-feiras, ele liberou o uso durante sua aula de laboratório de circuitos 2 apenas com a presença de poucos alunos do grupo de PI2.

## Reunião 08 - 24/04/2024
Data: 24/04/2024

Horário: 16h até 18h

Local: LDTEA

No dia 24 de abril de 2024, foi enviado um formulário aos integrantes do grupo Girassol com
o intuito de coletar suas expectativas e considerações sobre o andamento do projeto.
Abaixo, segue a orientação para o preenchimento do formulário, bem como as perguntas e
as respostas de forma resumida. Todos os 12 membros participaram.
A ATA do dia foi o próprio formulário.

“Este questionário tem o objetivo de entender a satisfação dos integrantes do grupo em
relação ao próprio grupo e o andamento do projeto como um todo.
Responda de forma sincera, para que o questionário seja eficiente e sirva como "norte" para
a melhora contínua do grupo. O questionário é anônimo!”
1. Os líderes (gerais e das áreas) têm sido claros e objetivos em suas comunicações e
tarefas? Se não, comente sobre.

    - Todos concordaram que sim.
    - Pontos de melhoria: maior comunicação entre lideranças e melhor divisão das
tarefas.

2. Você acredita que a comunicação dentro da equipe está boa? Se não, comente sobre.
    - A maioria concordaram que sim.
    - Pontos de melhoria: melhorar a integração, maior comunicação nas divisões e comunicação das tomadas de decisões no grupo geral.

3. Dentro da sua área, como está o seu desempenho? Comente um pouco sobre as suas
tarefas até então.
    - A maioria acredita que tem bom desempenho ou pode melhorar.
    - Comentários: por enquanto apenas envolvimento com documentação e
entendimento do problema. Mas a dependência de outras áreas atrasa o andamento da sub-área.

4. Você acredita que a divisão de tarefas está equilibrada? Se não, comente.
    - A maioria acredita que sim.
    - Comentários: alguns membros estão fazendo mais que outros, mas pelas circunstâncias do projeto.

5. Sobre o nosso cronograma, o que você acredita que pode ser feito para que possamos melhorar/aumentar o ritmo de trabalho do grupo como um todo? Dê sugestões
    - Gráficos de Gantt, Kanban e TrelloGit, e organizar melhor as etapas futuras.

6. Você se sente confortável para dar opiniões/sugestões no grupo em nossos encontros? Se não, comente.
    - Todos se sentem confortáveis.
    - Comentários: o grupo é unido, todos se ajudam, escutam e falam suas
contribuições. Alguns são mais tímidos que outros.

7. Você acredita que os professores estão ajudando suficientemente o grupo? O que você
acredita que poderia ser feito para manter um laço ainda mais estreito com os professores
para nos auxiliarem?
    - A maioria acredita que sim.
    - Comentários: os professores são dispostos e agregam ao projeto, mas, se
pudessem passar os 05 ao mesmo tempo no grupo seria mais efetivo.

8. Com o resultado de nosso trabalho até aqui, você acredita que o grupo alcançará boas
notas na disciplina?
    - Todos acreditam que estamos desempenhando acima da média.
    - Comentários: os membros contribuem ativamente, a documentação e
desenvolvimento estão num fluxo bom. O receio é a montagem física do projeto.

9. Utilize este espaço para fazer sugestões e/ou comentários quaisquer sobre nosso grupo
e o andamento do projeto. (Compilado dos comentários)
    - “Apesar do projeto ter demorado um pouco para caminhar, agora estamos bem”
    - “Gosto de nos reunir semanalmente, assim todos participam”
    - “A dinâmica do grupo é boa, só falta melhorar a comunicação técnica”
    - “Gosto da metodologia adotada pela nossa líder, para tocar o projeto”
    - “Nosso grupo é empenhado e dedicado, abraçamos a ideia. A liderança ajuda muito”

## Reunião 09 - 26/04/2024
Data: 26/04/2024

Horário: 14h até 18h

Local: Auditório e LDTEA

Presentes: Adrian, Camila, Daniel, Ingryd Karine, Hugo, João Paulo, Joaquim, Jorge, Marina, Pedro Henrique, Pedro Guimarães.

Ata feita por: Marina

### Objetivos
Tirar dúvidas com os professores, dar andamento às fases 2 e 3 do projeto e iniciar a construção da estrutura do *tracker*

### Deliberações
Equipe de estruturas - Início da construção da estrutura do *tracker*

Outras equipes - desenvolvimento das fases 2 e 3 do projeto

### Considerações finais

### Feedback dos Professores