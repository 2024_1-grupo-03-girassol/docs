# **Apêndice 01 - Aspectos de gerenciamento do projeto**

## **Termo de abertura do projeto**

### Nome do projeto: **Tracker Solar**

### Gestor do projeto: **Ingryd Karine**

**O gestor do projeto detém autoridade para tomar decisões e também
para definir as atividades a serem realizadas por cada membro, tanto em um
contexto geral quanto, se necessário, em um nível mais específico.**

---

### Descrição do projeto

&emsp; &emsp; O projeto visa criar um _tracker_ solar que otimize o desempenho das placas solares fotovoltaicas ao acompanhar o movimento do sol com um sistema de sensoriamento.
Além disso, fornece um sistema de notificações e registro de dados, fornecendo ao usuário
atualizações em tempo real sobre o status do _tracker_ e informações detalhadas
sobre seu funcionamento.

---

### Justificativa do projeto

'&emsp; &emsp; A demanda energética representa uma parte significativa do orçamento em diversos setores.
Nesse contexto, a implementação de sistemas fotovoltaicos próprios tem
ganhado popularidade em função do seu alto potencial de geração de energia elétrica.

&emsp; &emsp; Apesar do aumento da implementação de sistemas fotovoltaicos,
o investimento para tornar um ponto de consumo energeticamente autossustentável continua elevado. Isso se deve, pelo fato de que não conseguimos extrair o máximo potencial dos painéis fotovoltaicos e simultâneo a isso ainda é um sistema que tem seu valor elevado.

&emsp; &emsp; Assim, fica claro que uma proposta de tracker solar que busca
minimizar os custos de produção, oferecendo um desempenho otimizado para as placas e com mecanismos
que facilitem para o usuário o monitoramento de problemas técnicos e do desempenho do sistema, possui um alto valor no mercado.

---

### Objetivos do Projeto

&emsp; &emsp; O projeto tem como objetivo desenvolver um _tracker_ solar com baixo custo de
produção e alto desempenho na geração de energia. Além disso, busca-se integrar
um sistema de notificações que permita aos usuários acompanhar tanto problemas
técnicos quanto a produção do sistema ao longo do tempo.

---

### Stakeholders

#### Docentes da Disciplina

&emsp; &emsp; Os docentes têm um papel fundamental no acompanhamento e suporte
ao processo de desenvolvimento do projeto. Sua orientação é crucial para
garantir o controle eficaz do escopo e a seleção adequada de materiais e
tecnologias pelo grupo. Além disso, são responsáveis por avaliar a integração
das diversas áreas de engenharia nos marcos de controle e entregas do produto.

#### Integrantes do Grupo

&emsp; &emsp; Os integrantes do grupo são os principais agentes responsáveis pelo
desenvolvimento do _tracker_ solar, com a obrigação de cumprir os prazos definidos
na ementa da disciplina. Seu trabalho envolve não apenas a implementação do
projeto, mas também a definição detalhada do escopo, custos, arquitetura e
demais elementos essenciais para o sucesso do empreendimento.

#### Usuários

&emsp; &emsp; Os usuários serão produtores de energia conectados à rede elétrica.
Os mesmos desempenham um papel essencial nos ajustes de configurações e utilização eficiente do equipamento.
Além disso, devem monitorar atentamente as notificações emitidas pelo dispositivo,
garantindo um funcionamento contínuo e eficaz do sistema.

---

### Subprodutos do Projeto

#### Software

O subproduto de software deve proporcionar ao usuário um sistema de notificações
confiável e tolerante a falha, capaz de comunicar qualquer falha ou problema de
funcionamento do produto. Além disso, deve fornecer dados periodicamente ao usuário do sistema.

#### Estrutura

O subproduto de estrutura deve ser capaz de movimentar uma placa solar fotovoltaica de
aproximadamente 20 kg em 5 posições, visando manter a maior produção possível
na placa. Além disso, deve ser resiliente a condições climáticas desfavoráveis.

#### Eletrônica

O subproduto de eletrônica deve ser capaz de medir, em graus, o posicionamento
angular das 5 posições, validar a presença da luz do sol por meio do uso do LDR,
realizar o controle do giro do motor de passos, medir a tensão e a corrente produzida.

#### Energia

O subproduto de energia deve garantir uma alimentação estável e adequada para
manter todo o sistema do _tracker_ solar em pleno funcionamento.

## **Orçamento**

&emsp; &emsp; O orçamento do projeto foi calculado com base nas áreas.
Além disso, foram considerados os preços dos produtos encontrados no mercado,
juntamente com a soma dos serviços contratados para a execução do projeto.

### **Energia**

### **Eletrônica**

<center>

| <div style="width:300px">Produto</div> | Valor Unitário (R$) | Quantidade | Total (R$) |
| -------------------------------------- | ------------------- | ---------- | ---------- |
| Raspberry 3b+                          | 375.00              | 1          | 375.00     |
| Giroscópio                             | 25.00               | 1          | 25.00      |
| Driver do Motor                        | 70.00               | 1          | 70.00      |
| LDR                                    | 0.50                | 5          | 2.50       |
| Placa PCB                              | 25.00               | 2          | 50.00      |

</center>

### **Estrutura**

<center>

| <div style="width:300px">Produto</div> | Valor Unitário (R$) | Quantidade | Total (R$) |
| -------------------------------------- | ------------------- | ---------- | ---------- |
| Disco de corte                         | 3.00                | 1          | 3.00       |
| Disco flap                             | 7.50                | 2          | 15.00      |
| Lixa de ferro                          | 3.90                | 2          | 7.80       |
| Mancal Industrial                      | 40.79               | 2          | 81.58      |
| Acoplamento                            | 108.87              | 1          | 108.87     |
| Rolamento                              | 36.43               | 2          | 72.86      |

</center>

## **Lista É / Não É**

![Lista é não é](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/lista-e-nao-e.jpg)

## **Organização da equipe**

&emsp; A equipe de desenvolvimento está organizada em três áreas de especialização para maximizar a eficiência e a qualidade do trabalho. Cada área tem um líder dedicado que supervisiona as operações e garante a realização de metas e prazos. A estrutura organizacional visa promover a colaboração entre as equipes. 

- Software: liderada pelo João Paulo e contendo 5 desenvolvedores
- Energia/Eletrônica: liderada pelo Jorge Guilherme e contendo 1 desenvolvedores
- Estrutura: liderada por Camila Borba contendo apenas 1 desenvolvedor

&emsp; Além disso, Ingryd Karine é a coordenadora geral da equipe, proporcionando liderança e suporte em todos os aspectos do projeto. José Joaquim é o diretor de qualidade, garantindo que todos os padrões e procedimentos sejam rigorosamente seguidos para alcançar um produto final de excelência.

&emsp; A estrutura organizacional pode ser visualizada na figura a seguir, enquanto os dados individuais estão na tabela correspondente.

![Organograma da equipe de desenvolvimento](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/organograma.jpg)

<center>

| <div style="width:300px">Nome</div> | Matrícula | Curso |
| -------------------------------------- | ------------------- | ---------- | 
| Ingryd Karine Batista Bruno               | 160008531           | Engenharia de Energia   |
| Marina da Matta Nery                      | 200062450           | Engenharia de Energia   |
| José Joaquim da Silveira Araújo Junior    | 180123696           | Engenharia Eletrônica   | 
| Jorge Guilherme Bezerra Amaral            | 150013485           | Engenharia Eletrônica   | 
| Pedro Henrique Nogueira Bragança          | 190094478           | Engenharia de Software  | 
| João Paulo Lima da Silva                  | 190030755           | Engenharia de Software  | 
| João Gabriel Antunes                      | 170013651           | Engenharia de Software  | 
| Daniel Vinicius Ribeiro Alves             | 190026375           | Engenharia de Software  | 
| Hugo Rocha de Moura                       | 180136925           | Engenharia de Software  | 
| Adrian Soares Lopes                       | 160000572           | Engenharia de Software  |
| Camila de Oliveira Borba                  | 170161722           | Engenharia Aeroespacial |
| Pedro Henrique Guimarães de Souza Pereira | 190036486           | Engenharia Aeroespacial | 

</center>

<figure markdown="span">
  <figcaption>Organograma da equipe de desenvolvimento</figcaption>
    ![Organograma da equipe de desenvolvimento](https://gitlab.com/fga-pi2/semestre-2024-1/grupo-03/docs/-/raw/main/docs/assets/organograma.jpg){ width="800px" }
  <figcaption>Fonte: Autores</figcaption>
</figure>


## **Repositórios**

> Apresentar links para acesso aos repositórios do projeto.

## **EAP (Estrutura Analítica de Projeto) Geral do Projeto**

É com base na técnica de decomposição que se consegue dividir os
principais produtos do projeto em nível de detalhe suficiente para
auxiliar a equipe de gerenciamento do projeto na definição das
atividades do projeto.

Segundo [Tausworthe (1988)](https://www.sciencedirect.com/science/article/abs/pii/0164121279900189), a EAP é uma técnica de modelagem de sistemas e de gerenciamento de projetos que permite a representação gráfica da decomposição do trabalho a ser executado em um projeto. A EAP é uma ferramenta de gerenciamento de projetos que permite a visualização do trabalho a ser realizado em um projeto, facilitando a identificação de atividades, alocando recursos e estabelecendo responsabilidades.

A seguir estão dispostas as EAPs gerais e dos subsistemas que compõem o projeto.

### **EAP Geral 01**

![EAP Geral](/docs/assets/images/diagramas/diagrama-fase2-EAPGeral-1.jpg)

### **EAP Geral 02**

![EAP Geral](/docs/assets/images/diagramas/diagrama-fase2-EAPGeral-2.jpg)

### **EAP Software**

![EAP Geral](/docs/assets/images/diagramas/diagrama-fase2-EAPSoftware.jpg)

### **EAP Estrutura**

![EAP Geral](/docs/assets/images/diagramas/diagrama-fase2-EAPEstrutura.jpg)

### **EAP Energia**

![EAP Geral](/docs/assets/images/diagramas/diagrama-fase2-EAPEnergia.jpg)

### **EAP Eletrônica**

![EAP Geral](/docs/assets/images/diagramas/diagrama-fase2-EAPEletronica.jpg)

### **EAP Gerenciamento**

![EAP Geral](/docs/assets/images/diagramas/diagrama-fase2-EAPGerenciamento.jpg)


### **Definição de atividades e backlog das sprints**

Definir as atividades gerais que compõe o projeto e elaborar o
cronograma básico de execução. É importante identificar os responsáveis
pelas atividades.

## **Levantamento de riscos**

O gerenciamento eficaz de riscos é essencial para o sucesso de qualquer projeto, especialmente em empreendimentos complexos, como a implementação de um tracker solar. Este documento apresenta um plano abrangente para o gerenciamento de riscos, seguindo as diretrizes do Guia PMBOK(GUIDE, 2008). Este plano engloba a identificação, avaliação, monitoramento e ação contra os riscos associados ao projeto de tracker solar, visando garantir sua conclusão bem-sucedida e operação segura e eficiente.
As tabelas a seguir foram empregadas para categorizar os riscos em riscos gerais e específicos para cada área de atuação, proporcionando uma visão minuciosa e precisa após a identificação dos riscos em cada setor do projeto. Além disso, conduziu-se uma análise quantitativa, considerando a probabilidade, o impacto e a prioridade de cada risco.

### Tabela de Riscos Geral

| Risco                                                   | Consequência                          | Impacto | Medida a Tomar                                           |
| ------------------------------------------------------- | ------------------------------------- | ------- | -------------------------------------------------------- |
| Problemas de comunicação entre a equipe                 | Atraso no desenvolvimento do projeto  | Alto    | Prevenir gerindo as agendas de cada integrante           |
| Condições adversas nas entregas devido a greve          | Baixa qualidade do projeto            | Médio   | Prevenir montando um cronograma coerente com o semestre. |
| Atraso no prazo de entrega dos equipamento              | Atrasos no desenvolvimento do projeto | Alto    | Prevenir                                                 |
| Desistência da disciplina por parte de algum integrante | Sobrecarga dos membros restantes      | Alto    | Redistribuir as tarefas e ajustar o planejamento         |

### Tabela de Riscos de Software

| Risco                                       | Consequência                                            | Impacto    | Medida a Tomar                                              |
| ------------------------------------------- | ------------------------------------------------------- | ---------- | ----------------------------------------------------------- |
| Desafios com tecnologias de desenvolvimento | Atraso no desenvolvimento                               | Alto       | Realizar estudo das tecnologias selecionadas                |
| Falha na Integração com o sistema embarcado | Falha no funcionamento do tracker solar                 | Muito alto | Realizar estudo e testes do sistema embarcado               |
| Erro de implementação do Dashboard          | Falhas ou imprecisões no funcionamento do tracker solar | Muito alto | Estudar as tecnologias para o dashboard e sua implementação |
| Comprometimento dos Integrantes             | Atraso no desenvolvimento                               | Alto       | Manter equipe com tarefas bem distribuídas e monitoradas    |

### Tabela de Riscos de Estrutura

| Risco                                           | Consequência                                        | Impacto | Medida a Tomar                                                   |
| ----------------------------------------------- | --------------------------------------------------- | ------- | ---------------------------------------------------------------- |
| Dimensionamento inconsistente                   | Má alocação dos componentes                         | Alto    | Realizar dimensionamento preciso dos componentes                 |
| Má escolha de material                          | Degradação dos materiais e componentes              | Alto    | Conduzir simulação estática para validação do material escolhido |
| Fadiga causada por vibrações mecânicas          | Degradação dos materiais e componentes              | Alto    | Realizar simulações dinâmicas para análise de vibrações          |
| Exposição prolongada a altas temperaturas       | Degradação dos materiais e componentes              | Alto    | Implementar sistemas de refrigeração adequados                   |
| Interferência eletromagnética                   | Mau funcionamento dos componentes eletrônicos       | Alto    | Realizar testes de compatibilidade eletromagnética               |
| Desgaste devido a condições ambientais extremas | Deterioração dos componentes e redução da vida útil | Alto    | Implementar medidas de proteção e manutenção preventiva          |

### Tabela de Riscos de Energia e Eletrônica.

| Risco                                               | Consequência                                                        | Impacto    | Medida a Tomar                                                        |
| --------------------------------------------------- | ------------------------------------------------------------------- | ---------- | --------------------------------------------------------------------- |
| Falha no acionamento dos subsistemas                | Interrupção no fornecimento de energia ao projeto                   | Muito Alto | Verificação das conexões para evitar problemas de acionamento         |
| Incompatibilidade dos componentes                   | Funcionamento inadequado devido a divergências entre os componentes | Alto       | Verificação da compatibilidade para evitar problemas                  |
| Riscos de curto-circuito                            | Danos aos equipamentos elétricos devido a curtos-circuitos          | Alto       | Implementação de medidas para reduzir o risco de curto-circuito       |
| Queima dos componentes                              | Falha no funcionamento devido a componentes danificados             | Alto       | Adoção de medidas para minimizar o impacto de componentes danificados |
| Imprecisão ou atraso na comunicação                 | Interrupção ou atraso na transmissão de dados                       | Médio      | Implementação de redundância para garantir comunicação adequada       |
| Superaquecimento/Desgaste prematuro dos componentes | Redução da vida útil dos componentes devido a desgaste precoce      | Médio      | Realização de manutenções preventivas e monitoramento constante       |
